﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.IdentityModel.Protocols;

namespace DevOps535.Models
{
    public class BookDBHandle
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = "Server = tcp:dbserver535.database.windows.net,1433; Initial Catalog = db535; Persist Security Info = False; User ID = ravi; Password = Ap281853; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
            con = new SqlConnection(constring);
        }

        // **************** ADD NEW Book *********************
        public bool AddBook(bookManager smodel)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AddNewBook", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Author_Name", smodel.Author_Name);
            cmd.Parameters.AddWithValue("@Book_Title", smodel.Book_Title);
            cmd.Parameters.AddWithValue("@Category", smodel.Category);
            cmd.Parameters.AddWithValue("@Image_url", smodel.Image_url);
            cmd.Parameters.AddWithValue("@Price", smodel.Price);
            cmd.Parameters.AddWithValue("@display_expiry", "31-12-2018");
            

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        // ********** VIEW Book DETAILS ********************
        public List<bookManager> GetBooks()
        {
            connection();
            List<bookManager> booklist = new List<bookManager>();

            SqlCommand cmd = new SqlCommand("GetBookDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                booklist.Add(
                    new bookManager
                    {
                        Author_Name = Convert.ToString(dr["Author_Name"]),
                        Book_Title = Convert.ToString(dr["Book_Title"]),
                        Category = Convert.ToString(dr["Category"]),
                        Image_url = Convert.ToString(dr["Image_url"]),
                        Price = Convert.ToInt16(dr["Price"]),
                        display_expiry = Convert.ToDateTime(dr["display_expiry"]),
                        
                    });
            }
            return booklist;
        }

        // ***************** UPDATE BookT DETAILS *********************
        public bool UpdateDetails(bookManager smodel)
        {
            connection();
            SqlCommand cmd = new SqlCommand("UpdateNewBook", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Author_Name", smodel.Author_Name);
            cmd.Parameters.AddWithValue("@Book_Title", smodel.Book_Title);
            cmd.Parameters.AddWithValue("@Category", smodel.Category);
            cmd.Parameters.AddWithValue("@Image_url", smodel.Image_url);
            cmd.Parameters.AddWithValue("@Price", smodel.Price);
            cmd.Parameters.AddWithValue("@display_expiry", smodel.display_expiry);
            cmd.Parameters.AddWithValue("@book_id", smodel.book_id);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        // ********************** DELETE Book DETAILS *******************
        public bool DeleteBook(int book_id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("DeleteBook", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@book_id", book_id);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }
    }



}

