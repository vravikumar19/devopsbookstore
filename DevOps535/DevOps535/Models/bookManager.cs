﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DevOps535.Models
{
    public class bookManager
    {
        public String Author_Name { get; set; }
        public String Book_Title { get; set; }
        public string Category { get; set; }
        public string Image_url { get; set; }
        public int Price { get; set; }
        public DateTime display_expiry { get; set; }
        public int book_id { get; set; }
    }
}
