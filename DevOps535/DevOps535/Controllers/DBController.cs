﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevOps535.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DevOps535.Controllers
{
    public class DBController : Controller
    {
        // GET: DB
        public ActionResult Index()
        {

            BookDBHandle dbhandle = new BookDBHandle();
            ModelState.Clear();
            return View(dbhandle.GetBooks());
        }

        // GET: DB/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }



        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

        // 2. *************ADD NEW bookT ******************
        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(bookManager smodel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BookDBHandle sdb = new BookDBHandle();
                    if (sdb.AddBook(smodel))
                    {
                        ViewBag.AlertMsg = "Book Details Added Successfully";
                        ViewBag.Message = "Book Details Added Successfully";
                        ModelState.Clear();
                    }
                }
                return View();
            }
            catch
            {
                return View();
            }
        }



        //&&&&&&&&&&&&&&&&&&&&&&&&

        public ActionResult Edit(int id)
        {
            BookDBHandle sdb = new BookDBHandle();
            return View(sdb.GetBooks().Find(smodel => smodel.book_id == id));
        }

        // POST: Student/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, bookManager smodel)
        {
            try
            {
                BookDBHandle sdb = new BookDBHandle();
                sdb.UpdateDetails(smodel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

        // GET: DB/Delete/5
        // 4. ************* DELETE STUDENT DETAILS ******************
        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                BookDBHandle sdb = new BookDBHandle();
                if (sdb.DeleteBook(id))
                {
                    ViewBag.AlertMsg = "Book Deleted Successfully";
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}